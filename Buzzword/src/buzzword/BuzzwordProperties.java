package buzzword;

/**
 * Created by Christopher Delvalle on 11/13/16.
 */
public enum BuzzwordProperties {
    WORKSPACE_HEADING_LABEL,
    WORKSPACE_HEADING_LABEL2,
    ROOT_BORDERPANE_ID,
    TOP_TOOLBAR_ID,
    SEGMENTED_BUTTON_BAR,
    FIRST_TOOLBAR_BUTTON,
    LAST_TOOLBAR_BUTTON,
    HEADING_LABEL;
}
