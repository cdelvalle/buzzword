package buzzword;

import apptemplate.AppTemplate;
import components.AppComponentsBuilder;
import components.AppDataComponent;
import components.AppFileComponent;
import components.AppWorkspaceComponent;
import data.*;
import javafx.animation.AnimationTimer;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.geometry.HPos;
import javafx.geometry.Orientation;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.paint.Color;
import controller.BuzzwordController;
import gui.Workspace;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.effect.DropShadow;
import javafx.scene.effect.Glow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javafx.util.Duration;
import ui.AppMessageDialogSingleton;
import ui.YesNoCancelDialogSingleton;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Optional;
import java.util.TreeSet;

/**
 * Created by Christopher Delvalle on 11/13/16.
 */


public class Buzzword extends Application { //extends AppTemplate {

    private final String buttonStyle = "-fx-background-color: dimgray;\n" +
            "    -fx-background-insets: 0,1,2,3,0;\n" +
            "    -fx-text-fill: white;\n" +
            "    -fx-font-weight: bold;\n" +
            "    -fx-font-size: 14px;\n" +
            "    -fx-padding: 10 20 10 20;";

    private final String titleStyle = " -fx-font-size: 32px;\n" +
            "   -fx-font-family: \"Arial Black\";\n" +
            "   -fx-fill: #818181;\n" +
            "   -fx-effect: innershadow( three-pass-box , rgba(0,0,0,0.7) , 6, 0.0 , 0 , 2 );";


    private final String levelButtonStyle = "-fx-background-radius: 30px;" +
            "-fx-min-width: 60px;" +
            "-fx-min-height: 60px;" +
            "-fx-max-width: 60px;" +
            "-fx-max-height: 60px;"+
            "-fx-focus-color: rgb(106, 255, 100);"+
            "-fx-faint-focus-color: transparent;";

    private final String nodeStyle = "-fx-background-radius: 25px;" +
            "-fx-min-width: 50px;" +
            "-fx-min-height: 50px;" +
            "-fx-max-width: 50px;" +
            "-fx-max-height: 50px;"+
            "-fx-focus-color: rgb(255, 242, 42);"+
            "-fx-faint-focus-color: transparent;" +
            "-fx-base: dimgrey;" +
            "-fx-focus-color: transparent;" +
            "-fx-faint-focus-color: transparent;";

    private Button createButton;
    private Button loginButton;
    private Button logoutButton;
    private Button quitButton;
    private Button modeButton;
    private Button playButton;
    private Button startButton;
    private Button userButton;
    private Button homeButton;
    private Button helpButton;

    private BuzzwordController controller;
    private BorderPane sidebar;
    private BorderPane workArea;
    private BorderPane layout;

    private ToggleButton dictionaryWordsButton;
    private ToggleButton namesButton;
    private ToggleButton placesButton;

    private ToggleButton level1;
    private ToggleButton level2;
    private ToggleButton level3;
    private ToggleButton level4;

    private Dictionary dictionary;

    private String modeString;// = "Dictionary Words";
    public int level;// = 1;//TODO: move to gamedata, make private
    private String username;// = "";

    private int mode;// = 0;

    private GameData data;
    private GameDataFile file;


    private Label currentGuess;
    private Label guessedLabel;
    private Label scoreLabel;
    private Label totalLabel;
    //game data
    private boolean paused;
    private boolean selecting;
    private LetterButton currentNode;
    private ArrayList<LetterButton> selection;
    private ArrayList<ArrayList<LetterButton>> selections;
    private Boolean typing = false;
    private String typedText = "";
    private String selectedText = "";
    private ArrayList<String> words;
    private int time;
    private int score;
    //--

    private ArrayList<LetterButton> letterButtons;
    private Label timeRemaining;

    private Scene scene;
    private Long pauseMoment;

    private ArrayList<Dictionary> dictionaryList;

    private boolean win;
    private boolean timerAllowed;
    private boolean loggedIn = false;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setTitle("Buzzword");

//        Dictionary test = new Dictionary();
//        for(String s :test.getWordsSet()){
//            System.out.println(s);
//        }
        dictionaryList = new ArrayList<>();
        dictionaryList.add(new Dictionary("DictionaryWords"));
        dictionaryList.add(new Dictionary("Names"));
        dictionaryList.add(new Dictionary("ShakespeareWords"));
        dictionary = dictionaryList.get(0);

        controller = new BuzzwordController(this);
        data = new GameData(this);
        file = new GameDataFile();
        words = new ArrayList<String>();
        //sidebar
//        data.generateGrid();
        scene = initGUI();
        primaryStage.setScene(scene);
        primaryStage.show();

        Platform.setImplicitExit(false);
        primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent event) {
                event.consume();
                Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                alert.setTitle("Confirm exit");
                alert.setHeaderText("You are about to exit the application.");
                alert.setContentText("Are you sure you want to exit?");

                Optional<ButtonType> result = alert.showAndWait();
                if (result.get() == ButtonType.OK){
                    System.exit(0);
                }
            }
        });
    }

    public Pane defaultLayout(){
        resetWorkArea();
        //sidebar

//work area
        Label title = new Label("Buzzword!");
        title.setStyle(titleStyle+"-fx-font-size: 64px;");


        VBox topBox = new VBox();
        topBox.getChildren().addAll(title);
        topBox.setAlignment(Pos.TOP_CENTER);


        VBox sidebarBox = new VBox(5);
        sidebarBox.getChildren().addAll(helpButton,createButton,loginButton);

        sidebar.setTop(sidebarBox);
        sidebar.setBottom(quitButton);

        workArea.setTop(topBox);

        modeString = "Dictionary Words";
        level = 1;
        username = "";

        mode = 0;

        return layout;
    }

    public Pane loginLayout(){
        resetWorkArea();
        updateSidebar(loginButton);

        Label title = new Label("Login");
        title.setStyle(titleStyle);
        HBox topBox = new HBox();
        topBox.getChildren().addAll(title);
        topBox.setAlignment(Pos.CENTER);


        GridPane grid = new GridPane();
        grid.setAlignment(Pos.BASELINE_CENTER);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(25, 25, 25, 25));

        Label userName = new Label("User Name:");
        grid.add(userName, 0, 2);

        TextField userTextField = new TextField();
        grid.add(userTextField, 1, 2);

        Label pw = new Label("Password:");
        grid.add(pw, 0, 3);

        PasswordField pwBox = new PasswordField();
        grid.add(pwBox, 1, 3);

        Button signIn = new Button("Sign in");
        signIn.setStyle("-fx-background-color: dimgrey");
        signIn.setOnMouseClicked(e -> {

            username=userTextField.getText();
            data.setUsername(username);
            data.setPassword(Password.encrypt(pwBox.getText()));

            if(controller.handleReturningUser()){
                loggedIn = true;
                homeLayout();
            }
            else{
                //TODO: text:"Invalid username or password"
            }


        });

        HBox buttonBox = new HBox();
        buttonBox.setAlignment(Pos.BOTTOM_RIGHT);
        buttonBox.getChildren().add(signIn);
        grid.add(buttonBox,1,5);

        workArea.setTop(topBox);
        workArea.setCenter(grid);

        return layout;
    }

    public Pane createLayout(){
        resetWorkArea();
        updateSidebar(createButton);

        Label title = new Label("Create New Profile");
        title.setStyle(titleStyle);
        HBox topBox = new HBox();
        topBox.getChildren().addAll(title);
        topBox.setAlignment(Pos.CENTER);


        GridPane grid = new GridPane();
        grid.setAlignment(Pos.BASELINE_CENTER);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(25, 25, 25, 25));

        Label userName = new Label("Set User Name:");
        grid.add(userName, 0, 2);

        TextField userTextField = new TextField();
        grid.add(userTextField, 1, 2);

        Label pw = new Label("Set Password:");
        grid.add(pw, 0, 3);

        PasswordField pwBox = new PasswordField();
        grid.add(pwBox, 1, 3);

        Label pw2 = new Label("Confirm Password:");
        grid.add(pw2, 0, 4);

        PasswordField pwBox2 = new PasswordField();
        grid.add(pwBox2, 1, 4);

        Button create = new Button("Create");
        create.setStyle("-fx-background-color: dimgrey");
        create.setOnMouseClicked(e -> {
            username=userTextField.getText();
            if(pwBox.getText().equals(pwBox2.getText())) {
                data.setUsername(username);
                data.setPassword(Password.encrypt(pwBox.getText()));
                file.saveData(data);
                loggedIn = true;
                homeLayout();
            }
            pwBox.setText("");
            pwBox2.setText("");
//            if(!controller.handleNewUser()){}


        });

        HBox buttonBox = new HBox();
        buttonBox.setAlignment(Pos.BOTTOM_RIGHT);
        buttonBox.getChildren().add(create);
        grid.add(buttonBox,1,5);

        workArea.setTop(topBox);
        workArea.setCenter(grid);

        return layout;
    }

    public Pane profileLayout(){
        typing = false;
        selecting = false;
        resetWorkArea();

        updateSidebar(userButton);

        Label title = new Label(username+"'s Profile Page");
        title.setStyle(titleStyle);

        userButton.setText(username);

        VBox sideBarBox = new VBox(5);

        sideBarBox.getChildren().addAll(userButton, homeButton, modeButton,playButton);
        sideBarBox.setAlignment(Pos.TOP_CENTER);

        HBox topBox = new HBox();
        topBox.getChildren().addAll(title);
        topBox.setAlignment(Pos.CENTER);

        sidebar.setTop(sideBarBox);
        workArea.setTop(topBox);

        VBox bottomBox = new VBox(5);
        bottomBox.getChildren().addAll(logoutButton,quitButton);
        sidebar.setBottom(bottomBox);


        GridPane grid = new GridPane();
        grid.setAlignment(Pos.BASELINE_CENTER);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(25, 25, 25, 25));

        Label pwOld = new Label("Current Password:");
        grid.add(pwOld, 0, 2);

        PasswordField pwBoxOld = new PasswordField();
        grid.add(pwBoxOld, 1, 2);

        Label pw = new Label("Enter New Password:");
        grid.add(pw, 0, 3);

        PasswordField pwBox = new PasswordField();
        grid.add(pwBox, 1, 3);

        Label pw2 = new Label("Confirm New Password:");
        grid.add(pw2, 0, 4);

        PasswordField pwBox2 = new PasswordField();
        grid.add(pwBox2, 1, 4);

        Button changePW = new Button("Change Password");
        changePW.setStyle("-fx-background-color: dimgrey");
        grid.add(changePW,1,5);



        workArea.setCenter(grid);

        changePW.setOnMouseClicked(e -> {
            if(data.getPassword().equals(Password.encrypt(pwBoxOld.getText()))&&pwBox.getText().equals(pwBox2.getText())) {
                data.setPassword(Password.encrypt(pwBox.getText()));
                file.saveData(data);
            }else {
//                todo:invalid password
            }
            pwBoxOld.setText("");
            pwBox.setText("");
            pwBox2.setText("");
        });

        return layout;
    }

    public Pane homeLayout(){
        typing = false;
        selecting = false;

        resetWorkArea();
        updateSidebar(homeButton);

        Label title = new Label("Welcome, "+username+"!");
        title.setStyle(titleStyle);

        userButton.setText(username);

        VBox sideBarBox = new VBox(5);

        sideBarBox.getChildren().addAll(userButton, homeButton, modeButton,playButton);
        sideBarBox.setAlignment(Pos.TOP_CENTER);

        HBox topBox = new HBox();
        topBox.getChildren().addAll(title);
        topBox.setAlignment(Pos.CENTER);

        sidebar.setTop(sideBarBox);
        workArea.setTop(topBox);

        VBox bottomBox = new VBox(5);
        bottomBox.getChildren().addAll(logoutButton,quitButton,helpButton);
        sidebar.setBottom(bottomBox);
        return layout;
    }

    public Pane modeLayout(){
        resetWorkArea();
        updateSidebar(modeButton);

        final ToggleGroup gamemodeGroup = new ToggleGroup();
        Label title = new Label("Select mode: ");
        title.setStyle(titleStyle);

        HBox topBox = new HBox();
        topBox.getChildren().addAll(title);
        topBox.setAlignment(Pos.CENTER);

        dictionaryWordsButton = new ToggleButton("Dictionary Words");
        namesButton = new ToggleButton("Names");
        placesButton = new ToggleButton("Words Used by Shakespeare");


        dictionaryWordsButton.setStyle("-fx-base: dimgrey;-fx-focus-color: transparent;-fx-faint-focus-color: transparent;");
        dictionaryWordsButton.setPrefSize(288,50);
        namesButton.setStyle("-fx-base: dimgrey;-fx-focus-color: transparent;-fx-faint-focus-color: transparent;");
        namesButton.setPrefSize(288,50);
        placesButton.setStyle("-fx-base: dimgrey;-fx-focus-color: transparent;-fx-faint-focus-color: transparent;");
        placesButton.setPrefSize(288,50);
        setModeHandlers();

        dictionaryWordsButton.setToggleGroup(gamemodeGroup);
        namesButton.setToggleGroup(gamemodeGroup);
        placesButton.setToggleGroup(gamemodeGroup);

        VBox centerBox = new VBox(5);
        centerBox.setPadding(new Insets(40));
        centerBox.setAlignment(Pos.TOP_CENTER);
        centerBox.getChildren().addAll(dictionaryWordsButton,namesButton,placesButton);


        workArea.setTop(topBox);
        workArea.setCenter(centerBox);

        return layout;
    }

    public Pane levelSelectLayout(){
        resetWorkArea();
        updateProgress();
        updateSidebar(playButton);

        final ToggleGroup levelGroup = new ToggleGroup();

        Label title = new Label("Select level:");
        Label title2 = new Label(modeString);
        title.setStyle(titleStyle);
        title2.setStyle(titleStyle+" -fx-font-size: 20px;\n");

        VBox topBox = new VBox(50);
        topBox.getChildren().addAll(title,title2);
        topBox.setAlignment(Pos.CENTER);




        level1.setToggleGroup(levelGroup);
        level2.setToggleGroup(levelGroup);
        level3.setToggleGroup(levelGroup);
        level4.setToggleGroup(levelGroup);

        HBox levelBox = new HBox(40);
        levelBox.setAlignment(Pos.BOTTOM_CENTER);
        levelBox.setPadding(new Insets(0));
        levelBox.getChildren().addAll(level1,level2,level3,level4);

        VBox centerBox = new VBox(80);
        centerBox.setAlignment(Pos.CENTER);
        centerBox.getChildren().addAll(levelBox, startButton);

        workArea.setTop(topBox);
        workArea.setCenter(centerBox);

        return layout;
    }

    public Pane gameplayLayout(){
        resetWorkArea();
        resetGameData();

        Label title = new Label(modeString);
        title.setStyle(titleStyle);

        HBox currentGuessBox = new HBox();

        String labelText = selectedText;
        currentGuess = new Label(labelText.replace("", " ").trim());
        currentGuessBox.setStyle("-fx-background-color: lightgrey");
        currentGuessBox.getChildren().add(currentGuess);
        currentGuessBox.setPadding(new Insets(10));

        time = 100;
        timeRemaining = new Label("Time left: "+time);
        timeRemaining.setStyle(buttonStyle);//+ "-fx-font-size: 14px;");
        VBox topBox = new VBox(5);
        topBox.setAlignment(Pos.CENTER);
        BorderPane secondRow = new BorderPane();
        secondRow.setRight(timeRemaining);
        secondRow.setCenter(currentGuessBox);
        topBox.getChildren().addAll(title, secondRow);


        VBox bottomBox = new VBox(5);
        bottomBox.setAlignment(Pos.BOTTOM_CENTER.CENTER);
        Label levelLabel = new Label("Level "+level);
        levelLabel.setStyle(titleStyle);
        Button pauseButton = new Button("Pause");
        Button nextLevelButton = new Button((level ==4 ) ? "Finish" :"Next Level");

        pauseButton.setStyle(buttonStyle);
        nextLevelButton.setStyle(buttonStyle);



        StackPane gridStack = new StackPane();


        Pane linePane = new Pane();

        ArrayList<Rectangle> gridLinesH = new ArrayList<>();
        for(int i = 0; i<4; i++){
            for(int j = 0; j<3; j++) {
                Rectangle r = new Rectangle();

                r.setWidth(60);
                r.setHeight(3);
                r.setX(54.5 + 60 * j);
                r.setY(54.5 + 60 * i);
                gridLinesH.add(r);
            }
        }

        ArrayList<Rectangle> gridLinesV = new ArrayList<>();
        for(int i = 0; i<4; i++){
            for(int j = 0; j<3; j++) {
                Rectangle r = new Rectangle();

                r.setWidth(3);
                r.setHeight(60);
                r.setX(54.5 + 60 * i);
                r.setY(54.5 + 60 * j);
                gridLinesV.add(r);
            }
        }
        for(Rectangle r: gridLinesH){
            linePane.getChildren().add(r);
        }
        for(Rectangle r: gridLinesV){
            linePane.getChildren().add(r);
        }

        data.generateGrid(dictionary);
        LetterGraph graph = data.getGraph();

        letterButtons = new ArrayList<>();
        for(int i = 0; i <16; i++){
            Button node = new Button((graph.getNode(i).getLetter()+"").toUpperCase());
            node.setStyle(nodeStyle);
            letterButtons.add(new LetterButton(node, graph.getNode(i)));
        }

        for(int c = 0, i = 0; i<4; i++){
            for(int j = 0; j<4; j++,c++){
                letterButtons.get(c).getButton().setLayoutX(30 + 60 * j);
                letterButtons.get(c).getButton().setLayoutY(30 + 60 * i);
                linePane.getChildren().add(letterButtons.get(c).getButton());
            }
        }

        for(int i = 0,j=0; i<16; i++) {
            int l = (i%4)*4+i/4;
            if(i%4==0){
                letterButtons.get(i).addConnector(gridLinesH.get(j));
                letterButtons.get(l).addConnector(gridLinesV.get(j));
            }
            else if(i%4>0&&i%4<3){
                letterButtons.get(i).addConnector(gridLinesH.get(j));
                letterButtons.get(i).addConnector(gridLinesH.get(j+1));
                letterButtons.get(l).addConnector(gridLinesV.get(j));
                letterButtons.get(l).addConnector(gridLinesV.get(j+1));
                j++;
            }
            else{
                letterButtons.get(i).addConnector(gridLinesH.get(j));
                letterButtons.get(l).addConnector(gridLinesV.get(j));
                j++;
            }
        }
        //----
        VBox guessBox = new VBox(5);
        VBox scoreBox = new VBox(5);

        guessBox.setStyle(buttonStyle);
        guessBox.setPadding(new Insets(1));

        scoreBox.setStyle(buttonStyle);
        scoreBox.setPadding(new Insets(1));

        BorderPane rightPane = new BorderPane();

        guessedLabel = new Label("");
        scoreLabel = new Label("");
        totalLabel = new Label(0+"/"+data.getScoreCap());

        scoreLabel.setStyle("-fx-font-size: 8px;");
        guessedLabel.setStyle("-fx-font-size: 8px;");
        guessedLabel.setWrapText(true);

        VBox targetBox = new VBox(5);

        targetBox.setPadding(new Insets(1));
        targetBox.setAlignment(Pos.CENTER);

        Label targetLabel = new Label("Target: "+data.getTargetScore()+" points");///"+data.getScoreCap());

        targetBox.getChildren().add(targetLabel);
        targetLabel.setStyle(buttonStyle+"-fx-background-color: Transparent;");

        HBox totalBox = new HBox();
        totalLabel.setStyle(buttonStyle+"-fx-background-color: #3c3a38;");
        totalBox.setAlignment(Pos.CENTER_RIGHT);

        totalBox.getChildren().add(totalLabel);

        guessBox.setMinHeight(190);
        guessBox.setMaxWidth(125);
        guessBox.setMinWidth(125);
        guessBox.setPrefWidth(125);
        guessBox.getChildren().add(guessedLabel);


        scoreBox.setMinHeight(190);
        scoreBox.setMaxWidth(50);
        scoreBox.setMinWidth(50);
        scoreBox.setPrefWidth(50);
        scoreBox.getChildren().add(scoreLabel);

        VBox right = new VBox();
        right.setPrefHeight(190);
        right.setMaxHeight(190);
        right.setMinHeight(190);

        HBox topRight = new HBox();
        topRight.getChildren().addAll(guessBox,scoreBox);
        right.getChildren().addAll(targetBox,topRight,totalBox);
//        right.setAlignment(Pos.TOP_RIGHT);
        rightPane.setTop(right);
//        rightPane.setCenter(right);
//        rightPane.setCenter(guessBox);
//        rightPane.setBottom(totalBox);

//        guessedLabel.setText("                      ");
//        guessedLabel.setMaxWidth(guessedLabel.getWidth());
//        guessedLabel.setMaxHeight(guessedLabel.getHeight());
        ///
        VBox sideBarBox = new VBox(5);
        sideBarBox.getChildren().addAll(userButton, homeButton);
        sideBarBox.setAlignment(Pos.TOP_CENTER);
        sidebar.setTop(sideBarBox);
//
        workArea.setTop(topBox);
        workArea.setCenter(linePane);
        workArea.setBottom(bottomBox);
        workArea.setRight(rightPane);
        setPaused(false);
        pauseButton.setOnAction(e->{
            if(!getPaused()) {
                pauseMoment = System.currentTimeMillis();
                for(LetterButton btn: letterButtons){
                    btn.getButton().setStyle(nodeStyle + "-fx-font-size: 100px;");
                }
                pauseButton.setText("Resume");
                setPaused(true);
            }else{
                for(LetterButton btn: letterButtons){
                    btn.getButton().setStyle(nodeStyle);
                }
                pauseButton.setText("Pause");
                setPaused(false);
            }
        });

        HBox bottomButtons = new HBox(10);
        nextLevelButton.setDisable(true);
        bottomButtons.getChildren().addAll(pauseButton,nextLevelButton);
        bottomButtons.setAlignment(Pos.BOTTOM_CENTER);
        bottomBox.getChildren().addAll(levelLabel,bottomButtons);


        FlowPane solutionScreen = new FlowPane(Orientation.VERTICAL);
        solutionScreen.setColumnHalignment(HPos.LEFT); // align labels on left
        solutionScreen.setPrefWrapLength(200); // preferred height = 200
        solutionScreen.setPadding(new Insets(10, 10, 10,10));
        solutionScreen.setVgap(2);
        solutionScreen.setHgap(9);
        solutionScreen.setMaxWidth(280);
        solutionScreen.setPrefWidth(280);
        solutionScreen.setMinWidth(280);
        play(levelLabel,pauseButton,targetLabel,nextLevelButton,solutionScreen);
        return layout;
    }

    public void play(Label levelLabel,Button bottomButton,Label targetLabel,Button nextLevelButton,Pane solutionScreen) {
        setupGameHandlers();
        win = false;

        for(LetterButton lb: letterButtons){
            lb.getNode().setSelected(false);
        }
        long startTime = System.currentTimeMillis();


        timerAllowed = true;
        AnimationTimer timer = new AnimationTimer() {
            int timePaused = 0;
            int totalTimePaused = 0;

            @Override
            public void handle(long now) {
                //handling typing
                scene.setOnKeyTyped((KeyEvent event) -> {
                    handleKeyTyped(event);
                });

                //updating the current guess bar at the top
                currentGuess.setText(selectedText.replace("", " ").trim().toUpperCase());

                //timer stuff
                long currentTime = System.currentTimeMillis();
                if(paused)
                    timePaused = (int)((((currentTime - pauseMoment) / 1000.0)));
                else{
                    totalTimePaused += timePaused;
                    timePaused = 0;
                }
                time = (int)(60-(((currentTime - startTime) / 1000.0))+timePaused+totalTimePaused);
                if(!paused)
                    timeRemaining.setText("Time left: "+time);
                //

                //making the connectors glow/not glow
                updateConnectorGlow();
                if(win){
                    DropShadow glow = new DropShadow();
                    glow.setColor(Color.LIMEGREEN);
                    levelLabel.setText("Good Job!");
                    levelLabel.setTextFill(Color.LIMEGREEN);
                    levelLabel.setEffect(glow);

                    nextLevelButton.setDisable(false);

                    targetLabel.setTextFill(Color.LIMEGREEN);
                    targetLabel.setEffect(glow);
                }
                if(time==0) {
                    deselectAll();
                    stop(true);
                }
                if(!timerAllowed){
                    super.stop();
                }
            }

//            @Override
            public void stop(boolean outOfTime) {
                super.stop();
                if(outOfTime) {
                    String highscore = "HIGH SCORE: ";
                    boolean newHigh = false;
                    Label bestLabel = new Label();
                    switch(mode){
                        case 0:
                            if(data.setDictionaryBest(level-1,score+0)) {
                                System.out.println("???");
                                newHigh = true;
                                highscore = "NEW "+highscore;
                            }
                            highscore = highscore+data.getDictionaryBest(level-1);
                            break;
                        case 1:
                            if(data.setNamesBest(level-1,score)) {
                                newHigh = true;
                                highscore = "NEW "+highscore;
                            }
                            highscore = highscore+data.getNamesBest(level-1);
                            break;
                        case 2:
                            if(data.setShakespearBest(level-1,score)) {
                                newHigh = true;
                                highscore = "NEW "+highscore;
                            }
                            highscore = highscore+data.getShakespearBest(level-1);
                            break;
                    }

                    VBox gameoverBox = new VBox(3);
                    bestLabel.setText(highscore);
                    bestLabel.setAlignment(Pos.CENTER);
                    bestLabel.setStyle("-fx-font-size: 25px");
                    if(newHigh){
                        DropShadow glow = new DropShadow();
                        glow.setColor(Color.GOLD);
                        bestLabel.setTextFill(Color.GOLD);
                        bestLabel.setEffect(glow);
                    }
                    newHigh = false;
                    ArrayList<String> solutionWords = new ArrayList<>();
                    solutionWords.addAll(data.getPossibleWords());
                    Collections.sort(solutionWords,new LengthSort());
                    for (String s : solutionWords) {
                        Label temp = new Label(s);
                        temp.setStyle("-fx-font-size: 10px;");
                        DropShadow glow = new DropShadow();
                        glow.setColor(Color.LIMEGREEN);
                        if (words.contains(s)) {
                            temp.setTextFill(Color.LIMEGREEN);
                            temp.setEffect(glow);
                        }
                        solutionScreen.getChildren().add(temp);
                    }

                    gameoverBox.getChildren().addAll(bestLabel,solutionScreen);
                    workArea.setCenter(gameoverBox);
                    if (!win) {
                        DropShadow glow = new DropShadow();
                        glow.setColor(Color.RED);
                        levelLabel.setTextFill(Color.RED);
                        levelLabel.setText("Game Over");
                        levelLabel.setEffect(glow);
                    } else {
                        DropShadow glow = new DropShadow();
                        glow.setColor(Color.LIMEGREEN);
                        levelLabel.setTextFill(Color.LIMEGREEN);
                        levelLabel.setText("Good Job!");
                        levelLabel.setEffect(glow);
                    }
                    bottomButton.setText("Replay");
                    bottomButton.setOnAction(e -> {

                        gameplayLayout();
                    });
                    System.out.println("game over");
                }
            }
        };

        nextLevelButton.setOnAction(e->{
            deselectAll();
            resetWorkArea();
            if(level>=4) {
                controller.handleHomeRequest();
            }
            else if(level == 1){
                level1.setSelected(false);
                level2.setSelected(true);
                level++;
                gameplayLayout();
            }
            else if(level == 2){
                level2.setSelected(false);
                level3.setSelected(true);
                level++;
                gameplayLayout();
            }
            else if(level == 3){
                level3.setSelected(false);
                level4.setSelected(true);
                level++;
                gameplayLayout();
            }
            timer.stop();
        });
        homeButton.setOnMouseClicked(e -> {
            controller.handleHomeRequest();
            timer.stop();

        });
        logoutButton.setOnMouseClicked(e -> {
            loggedIn = false;
            controller.handleLogoutRequest();
            timer.stop();
        });
        userButton.setOnAction(e->{
            controller.handleProfileRequest();
            timer.stop();
        });

        timer.start();
    }

    public void setupGameHandlers(){
        selection = new ArrayList<>();
        selections = new ArrayList<>();
        for(LetterButton btn: letterButtons){
            btn.getButton().setOnMousePressed(e->{
                if(!typing&&!paused) {

                    //select by click
                    btn.getNode().setSelected(true);
                    currentNode = btn;
                    selectedText = selectedText + btn.getNode().getLetter();
                    selection.add(currentNode);
                    selections.add(selection);
                    selecting = true;
                }
            });
            btn.getButton().setOnDragDetected(e->{
                if(!typing&&!paused) {
                    scene.startFullDrag();
                }
            });
            btn.getButton().setOnMouseDragOver(e->{
                if(!typing&&!paused&&currentNode!=null) {
                    if(currentNode.getNode().contains(btn.getNode())&&!btn.getNode().getSelected()) {

                        //select by dragging
                        btn.getNode().setSelected(true);
                        selectedText = selectedText + btn.getNode().getLetter();
                        currentNode = btn;
                        selection.add(currentNode);
                    }
                    else if(!(currentNode==btn) && selection.size()>1&&selection.get(selection.size()-2) == btn && btn.getNode().getSelected()){

                        //deselect
                        currentNode.getNode().setSelected(false);
                        for(Rectangle r: currentNode.getConnectors()){
                            r.setEffect(null);
                            r.setFill(Color.BLACK);
                        }
                        selectedText = selectedText.substring(0, selectedText.length()-1);
                        selection.remove(currentNode);
                        currentNode = btn;
                    }
                }
            });
            btn.getButton().setOnMouseReleased(e->{
                if(!typing) {
                    btn.getNode().setSelected(false);
                    currentNode = null;
                    selection.removeAll(selection);

                    if (data.guessWord(selectedText)) {
                        addWord(selectedText);
                    }

                    selectedText = "";
                    selections.remove(selection);
                    selecting = false;
                }
            });
        }
    }

    public void addWord(String text){
        if(!words.contains(text)) {
            words.add(text);

            int points;
            if(text.length()<=4)
                points=1;
            else if(text.length()==5)
                points=2;
            else if(text.length()==6)
                points=3;
            else  if(text.length()==7)
                points=5;
            else
                points=11;

            score+=points;
            guessedLabel.setText((text + "\n" + guessedLabel.getText()).toUpperCase());
            scoreLabel.setText(points+ "\n" + scoreLabel.getText());
            totalLabel.setText(score+"/"+data.getScoreCap());
            if(score >= data.getTargetScore()){
                win = true;
                if(level<4){
                    switch(mode){
                        case 0:
                            data.setDictionaryProgress(level,true);
                            data.setDictionaryBest(level-1,score);
                            break;
                        case 1:
                            data.setNamesProgress(level,true);
                            data.setNamesBest(level-1,score);
                            break;
                        case 2:
                            data.setShakespearProgress(level,true);
                            data.setShakespearBest(level-1,score);
                            break;
                    }
                }else{
                    switch(mode) {
                        case 0:
                            data.setDictionaryBest(level - 1, score);
                            break;
                        case 1:
                            data.setNamesBest(level - 1, score);
                            break;
                        case 2:
                            data.setShakespearBest(level - 1, score);
                            break;
                    }
                }
                file.saveData(data);
            }
        }
    }

    private void setupHandlers() {

        createButton.setOnMouseClicked(e -> {

            controller.handleCreateRequest();

        });

        userButton.setOnAction(e->{
            controller.handleProfileRequest();
        });

        loginButton.setOnMouseClicked(e -> {
            controller.handleLoginRequest();

        });

        logoutButton.setOnMouseClicked(e -> controller.handleLogoutRequest());

        quitButton.setOnMouseClicked(e -> controller.handleQuitRequest());

        playButton.setOnMouseClicked(e -> {
            controller.handlePlayRequest();

        });

        homeButton.setOnMouseClicked(e -> {
            controller.handleHomeRequest();

        });

        helpButton.setOnMouseClicked(e -> {
            showHelp();

        });

        modeButton.setOnMouseClicked(e -> {
            controller.handleModeRequest();

        });

        setModeHandlers();


        level1.setOnMouseClicked(e -> level = 1);

        level2.setOnMouseClicked(e -> level = 2);

        level3.setOnMouseClicked(e -> level = 3);

        level4.setOnMouseClicked(e -> level = 4);

        startButton.setOnMouseClicked(e -> controller.handleStartRequest());

    }
    private void setModeHandlers(){
        dictionaryWordsButton.setOnMouseClicked(e -> {
            dictionary = dictionaryList.get(0);
            modeString = "Dictionary Words";
            mode = 0;
        });

        namesButton.setOnMouseClicked(e -> {
            dictionary = dictionaryList.get(1);
            modeString = "Names";
            mode = 1;
        });

        placesButton.setOnMouseClicked(e -> {
            dictionary = dictionaryList.get(2);
            modeString = "Shakespeare";
            mode = 2;
        });
    }
    private void resetWorkArea(){
        workArea.setBottom(null);
        workArea.setCenter(null);
        workArea.setTop(null);
        workArea.setLeft(null);
        workArea.setRight(null);
    }

    private void updateSidebar(Button btn){
        ArrayList<Button> btnList= new ArrayList<>();

        btnList.add(createButton);
        btnList.add(loginButton);
        btnList.add(modeButton);
        btnList.add(playButton);
        btnList.add(userButton);
        btnList.add(homeButton);

        for(int i = 0; i < btnList.size(); i++){
            if (btn.equals(btnList.get(i))){
                btnList.get(i).setDisable(true);
            }
            else
                btnList.get(i).setDisable(false);
        }
    }


    public void resetData(){
        data = new GameData(this);
    }

    public Scene initGUI(){
        sidebar = new BorderPane();
        sidebar.setStyle("-fx-background-color: darkgray;");
        sidebar.setPadding(new Insets(5));

        //area to right of sidebar
        workArea = new BorderPane();
        workArea.setStyle("-fx-background-color: gray");
        workArea.setPadding(new Insets(5));

        //gamemode setup
        dictionaryWordsButton = new ToggleButton("Dictionary Words");
        namesButton = new ToggleButton("Names");
        placesButton = new ToggleButton("Words Used by Shakespeare");

        dictionaryWordsButton.setSelected(true);
        dictionaryWordsButton.setStyle("-fx-base: dimgrey;-fx-focus-color: transparent;-fx-faint-focus-color: transparent;");
        dictionaryWordsButton.setPrefSize(288,50);
        namesButton.setStyle("-fx-base: dimgrey;-fx-focus-color: transparent;-fx-faint-focus-color: transparent;");
        namesButton.setPrefSize(288,50);
        placesButton.setStyle("-fx-base: dimgrey;-fx-focus-color: transparent;-fx-faint-focus-color: transparent;");
        placesButton.setPrefSize(288,50);
        dictionaryWordsButton.setSelected(true);
        //level setup
        level1 = new ToggleButton("1");
        level2 = new ToggleButton("2");
        level3 = new ToggleButton("3");
        level4 = new ToggleButton("4");

        level1.setStyle(levelButtonStyle);
        level2.setStyle(levelButtonStyle);
        level3.setStyle(levelButtonStyle);
//level four disabled for demonstration
        level4.setStyle(levelButtonStyle);

        level1.setSelected(true);
        startButton = new Button("Start!");
        startButton.setStyle(buttonStyle);
        //setup default homescreen
        userButton = new Button();
        createButton = new Button();
        loginButton = new Button();
        quitButton = new Button();
        modeButton = new Button();
        playButton = new Button();
        logoutButton = new Button();
        homeButton = new Button();

        helpButton = new Button();

        layout = new BorderPane();



        userButton.setStyle(buttonStyle+
                "-fx-background-color: #3c3a38;");


        createButton.setStyle(buttonStyle);
        loginButton.setStyle(buttonStyle);
        quitButton.setStyle(buttonStyle);
        modeButton.setStyle(buttonStyle);
        playButton.setStyle(buttonStyle);
        logoutButton.setStyle(buttonStyle);
        homeButton.setStyle(buttonStyle);

        helpButton.setStyle(buttonStyle+"-fx-font-size: 8;");
        helpButton.setPrefSize(144,0);

        userButton.setPrefSize(144,40);
        createButton.setPrefSize(144,40);
        loginButton.setPrefSize(144,40);
        quitButton.setPrefSize(144,40);
        modeButton.setPrefSize(144,40);
        playButton.setPrefSize(144,40);
        logoutButton.setPrefSize(144,40);
        homeButton.setPrefSize(144,40);

        createButton.setText("Create Profile");
        loginButton.setText("Sign in");
        modeButton.setText("Select Mode");
        playButton.setText("Play!");
        quitButton.setText("Quit");
        logoutButton.setText("Sign out");
        homeButton.setText("Home");

        helpButton.setText("Help");

        layout.setCenter(workArea);
        layout.setLeft(sidebar);

        defaultLayout();

        //setup handlers
        setupHandlers();



        //show application
        Scene scene = new Scene(layout, 640, 480);
        setupShortcuts(scene);
        return scene;
    }

    private void updateProgress(){
        level1.setDisable(false);//data.getProgress()[mode][0]);
        level2.setDisable(!data.getProgress()[mode][1]);
        level3.setDisable(!data.getProgress()[mode][2]);
        level4.setDisable(!data.getProgress()[mode][3]);
    }

    public GameData getData(){return data;}

    public GameDataFile getFile(){return file;}

    public static void showHelp(){
        Stage helpStage = new Stage();

        ScrollPane helpText = new ScrollPane();
        Scene scene = new Scene(helpText, 400, 400);
        Text text = new Text("Welcome to Buzzword!\n\n"+
                "First, create a profile or log in\n"+
                "Next select a game mode, select a level and play!\n" +
                "\nThere are 3 game modes to choose from:\n" +
                "1. Dictionary words: All words that you can find in a standard english dictionary\n" +
                "2. Names: All common names of people\n" +
                "3. Shakespeare: All words used in any of the words of William Shakespeare\n\n" +
                "There are 4 levels for each category, each one requires you to getter a higher" +
                " percentage of the possible score."+
                "\nDuring gameplay you can click and drag on letters or type. " +
                "When you type you will see all nodes that fit what you've typed light up"+
                "If what you've highlighted is a valid word, it will be added to the list of guessed words and scored\n"+
                "\nThe score is based on the following criteria:\n" +
                "1. If the word is less than 3 letters long, it is not a valid word\n" +
                "2. If the word is 4 or 3 letters long, it is worth 1 point.\n" +
                "3. If the word is 5 letters long it is worth 2 points.\n" +
                "4. If the word is 6 letters long it is worth 3 points.\n" +
                "5. If the word is 7 letters long it is worth 5 points.\n" +
                "6. If the word is greater than 7 letters long it is worth 11 points" +
                "\n" +
                "\n" +
                "Go as fast as possible because you only have 60 seconds to score as many points as you can!");
        text.wrappingWidthProperty().bind(scene.widthProperty());
        helpText.setFitToWidth(true);
        helpText.setContent(text);

        helpStage.setScene(scene);
        helpStage.show();
    }

    public boolean getPaused(){
        return paused;
    }
    public void setPaused(boolean paused){
        this.paused = paused;
    }
    public void handleKeyTyped(KeyEvent event){
        if(!paused&&!selecting) {
            char input = '0';
            if (event.getCharacter().length() == 0) {
                if (typing) {
                    typedText = typedText.substring(0, typedText.length() - 1);
                    selectedText = typedText;
                    if (typedText.length() > 0) {
                        input = typedText.charAt(typedText.length() - 1);
                    } else {
                        typing = false;
                        selections.removeAll(selections);
                        for (LetterButton lb : letterButtons) {
                            lb.getNode().setSelected(false);
                        }

                        typedText = "";
                        selectedText = "";
                        input = '0';
                    }
                }
            } else{
                input = event.getCharacter().charAt(0);
                if(Character.isAlphabetic(input))
                    typedText = typedText + input;
            }

            if (!selecting && Character.isAlphabetic(input)) {
                typing = true;
                selections = new ArrayList<>();
                for (LetterButton lb : letterButtons) {
                    lb.getNode().setSelected(false);
                    if (typedText.charAt(0) == lb.getNode().getLetter()) {
                        ArrayList<ArrayList<LetterNode>> paths = LetterGraph.findSelections(lb.getNode(), typedText);
                        if (paths != null) {
                            for (ArrayList<LetterNode> nodes : paths) {
                                ArrayList<LetterButton> path = new ArrayList<LetterButton>();
                                for (LetterNode node : nodes) {
                                    node.setSelected(true);
                                    path.add(node.getLetterButton());
                                }
                                if (!path.isEmpty()) {
                                    selections.add(path);
                                }
                            }
                        }
                    }
                }
                selectedText = typedText;
            } else if (Character.isWhitespace(input)&&input!=' ') {
                typing = false;
                selections.removeAll(selections);

                if (data.guessWord(selectedText)) {
                    addWord(selectedText);
                }
                for (LetterButton lb : letterButtons) {
                    lb.getNode().setSelected(false);
                }

                typedText = "";
                selectedText = "";
            }
        }

    }
    public void updateConnectorGlow(){
        DropShadow glow = new DropShadow();
        glow.setSpread(.8);
        glow.setWidth(15);
        glow.setHeight(15);
        glow.setColor(Color.LIMEGREEN);
        for(LetterButton lb: letterButtons){
            if(selecting||typing) {
                if (lb.getNode().getSelected()) {
                    lb.getButton().setEffect(glow);
                } else {
                    lb.getButton().setEffect(null);
                }
            }else{
                for(Rectangle r :lb.getConnectors()){
                    r.setEffect(null);
                    r.setFill(Color.BLACK);
                }
                lb.getButton().setEffect(null);
                lb.getNode().setSelected(false);
            }
        }
        for(LetterButton lb:letterButtons) {
            for (Rectangle r : lb.getConnectors()) {
                r.setEffect(null);
                r.setFill(Color.BLACK);
            }
        }
        if(!selections.isEmpty()) {
            for(ArrayList<LetterButton> s: selections) {
                boolean b = true;
                for(LetterButton lb: s){
                    lb.getNode().setSelected(true);
                }
                if(b) {

                    for (int i = 1; i < s.size(); i++) {
                        Rectangle r = s.get(i).hasCommonConnector(s.get(i - 1));//!=null);
                        if (r != null) {
                            r.setEffect(glow);
                            r.setFill(Color.LIMEGREEN);
                        }

                    }
                }else break;
            }
        }else {
        }

    }
    public void resetGameData(){
        selectedText = "";
        typedText = "";
        score = 0;
        paused = false;
        selecting = false;
        typing = false;
        currentNode = null;
        selection = new ArrayList<>();
        selections = new ArrayList<>();

    }
    public void deselectAll(){
        for(LetterButton lb: letterButtons){
            for(Rectangle r: lb.getConnectors()){
                r.setFill(Color.BLACK);
                r.setEffect(null);
            }
            lb.getButton().setEffect(null);
        }
    }
//    public ArrayList<ArrayList<LetterButton>> findSelections(String word){
//        ArrayList<ArrayList<LetterButton>> list = new ArrayList<>();
//        for(LetterButton lb: letterButtons) {
//            ArrayList<ArrayList<LetterButton>> temp = null;
//            if(lb.getNode().getLetter()==word.charAt(0)) {
//                temp = findSelection(lb.getNode(), word);
//            }
//            if(temp!=null){
//                list.addAll(temp);
//            }
//        }
//        return list;
//    }
//    public ArrayList<ArrayList<LetterButton>> findSelection(LetterNode node, String word) {
//        ArrayList<ArrayList<LetterButton>> list = new ArrayList<>();
//        for(LetterNode ln: node.getNodes()) {
//            if (ln!=null&&ln.getLetter() == word.charAt(0)) {
//                ArrayList<LetterButton> temp = new ArrayList<>();
//                findSelection(ln,word,temp,1);
//                if(temp!=null){
//                    list.add(temp);
//                }
//            }
//        }
//        return list;
//    }
//
//    public ArrayList<LetterButton> findSelection(LetterNode node, String word,ArrayList<LetterButton> list, int index) {
//        for(LetterNode ln: node.getNodes()) {
//            if (ln!=null&&ln.getLetter() == word.charAt(index)) {
//                findSelection(ln,word,(ArrayList<LetterButton>)list.clone(),index++);
//            }
//        }
//        return null;

    public void setupShortcuts(Scene scene){
        scene.getAccelerators().put(new KeyCodeCombination(KeyCode.C, KeyCombination.CONTROL_DOWN),new Runnable() {
            @Override
            public void run() {
                if(!loggedIn)
                controller.handleCreateRequest();
            }
        });

        scene.getAccelerators().put(new KeyCodeCombination(KeyCode.P, KeyCombination.CONTROL_DOWN),new Runnable() {
            @Override
            public void run() {
                if(loggedIn)
                controller.handleProfileRequest();
            }
        });


        scene.getAccelerators().put(new KeyCodeCombination(KeyCode.L, KeyCombination.CONTROL_DOWN),new Runnable() {
            @Override
            public void run() {
                if(!loggedIn)
                controller.handleLoginRequest();
            }
        });

        scene.getAccelerators().put(new KeyCodeCombination(KeyCode.W, KeyCombination.CONTROL_DOWN),new Runnable() {
            @Override
            public void run() {
                if(loggedIn) {
                    loggedIn = false;
                    controller.handleLogoutRequest();
                }
            }
        });

        scene.getAccelerators().put(new KeyCodeCombination(KeyCode.Q, KeyCombination.CONTROL_DOWN),new Runnable() {
            @Override
            public void run() {
                controller.handleQuitRequest();
            }
        });

        scene.getAccelerators().put(new KeyCodeCombination(KeyCode.N, KeyCombination.CONTROL_DOWN),new Runnable() {
            @Override
            public void run() {
                if(loggedIn) {
                    controller.handleHomeRequest();
                    controller.handlePlayRequest();
                }
            }
        });

        scene.getAccelerators().put(new KeyCodeCombination(KeyCode.SPACE, KeyCombination.CONTROL_DOWN),new Runnable() {
            @Override
            public void run() {
                if(loggedIn)
                controller.handleHomeRequest();
            }
        });

        scene.getAccelerators().put(new KeyCodeCombination(KeyCode.H, KeyCombination.CONTROL_DOWN),new Runnable() {
            @Override
            public void run() {
                showHelp();
            }
        });

        scene.getAccelerators().put(new KeyCodeCombination(KeyCode.M, KeyCombination.CONTROL_DOWN),new Runnable() {
            @Override
            public void run() {
                if(loggedIn) {
                    controller.handleHomeRequest();
                    controller.handleModeRequest();
                }
            }
        });


    }
    public void setTimerAllowed(boolean timerAllowed){
        this.timerAllowed = timerAllowed;
    }
}





  /*  public String getFileControllerClass() {
        return "BuzzwordController";
    }

    @Override
    public AppComponentsBuilder makeAppBuilderHook() {
        return new AppComponentsBuilder() {
            @Override
            public AppDataComponent buildDataComponent() throws Exception {
                return new GameData(Buzzword.this);
            }

            @Override
            public AppFileComponent buildFileComponent() throws Exception {
                return new GameDataFile();
            }

            @Override
            public AppWorkspaceComponent buildWorkspaceComponent() throws Exception {
                return new Workspace(Buzzword.this);
            }
        };
    }*/

