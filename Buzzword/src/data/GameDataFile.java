package data;

import com.fasterxml.jackson.core.*;
import components.AppDataComponent;
import components.AppFileComponent;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Set;

/**
 * Created by Christopher Delvalle on 11/13/16.
 */
public class GameDataFile {//implements AppFileComponent {
    final String PASSWORD = "PASSWORD";
    final String DICTIONARY_PROGRESS = "DICTIONARY_PROGRESS";
    final String NAMES_PROGRESS = "NAMES_PROGRESS";
    final String PLACES_PROGRESS = "PLACES_PROGRESS";

    final String DICTIONARY_BEST = "DICTIONARY_BEST";
    final String NAMES_BEST = "NAMES_BEST";
    final String PLACES_BEST = "PLACES_BEST";

    public boolean saveData(GameData data) {
        GameData       gamedata    = data;
        Path to = Paths.get("./users/"+data.getUsername()); //TODO: system to prevent overwriting user
        String password = gamedata.getPassword();
        boolean[] dictionaryProgress = gamedata.getProgress()[0];
        boolean[] namesProgress = gamedata.getProgress()[1];
        boolean[] placesProgress = gamedata.getProgress()[2];

        int[] DictionaryBest = gamedata.getBest()[0];
        int[] NamesBest = gamedata.getBest()[1];
        int[] placesBest = gamedata.getBest()[2];


//        Set<Character> badguesses  = gamedata.getBadGuesses();
//
        JsonFactory jsonFactory = new JsonFactory();
//
        try (OutputStream out = Files.newOutputStream(to)) {
//
            JsonGenerator generator = jsonFactory.createGenerator(out, JsonEncoding.UTF8);
//
            generator.writeStartObject();
//
            generator.writeStringField(PASSWORD, gamedata.getPassword());
//
            generator.writeFieldName(DICTIONARY_PROGRESS);
            generator.writeStartArray(dictionaryProgress.length);
            for (boolean b : dictionaryProgress)
                generator.writeString(Boolean.toString(b));
            generator.writeEndArray();

            generator.writeFieldName(NAMES_PROGRESS);
            generator.writeStartArray(namesProgress.length);
            for (boolean b : namesProgress)
                generator.writeString(Boolean.toString(b));
            generator.writeEndArray();

            generator.writeFieldName(PLACES_PROGRESS);
            generator.writeStartArray(placesProgress.length);
            for (boolean b : placesProgress)
                generator.writeString(Boolean.toString(b));
            generator.writeEndArray();

            generator.writeFieldName(DICTIONARY_BEST);
            generator.writeStartArray(DictionaryBest.length);
            for (int i : DictionaryBest)
                generator.writeString(Integer.toString(i));
            generator.writeEndArray();
//highscores
            generator.writeFieldName(NAMES_BEST);
            generator.writeStartArray(NamesBest.length);
            for (int i : NamesBest)
                generator.writeString(Integer.toString(i));
            generator.writeEndArray();

            generator.writeFieldName(PLACES_BEST);
            generator.writeStartArray(placesBest.length);
            for (int i : placesBest)
                generator.writeString(Integer.toString(i));
            generator.writeEndArray();

            generator.writeEndObject();

            generator.close();

        } catch (IOException e) {
            e.printStackTrace();
            System.exit(1);
        }
        return true;
    }


    public boolean loadData(GameData data) throws IOException {
        String password = "";
        GameData gamedata = (GameData) data;
        gamedata.reset();

        Path from = Paths.get("./users/"+data.getUsername());
        JsonFactory jsonFactory = new JsonFactory();
        JsonParser jsonParser  = jsonFactory.createParser(Files.newInputStream(from));

        while (!jsonParser.isClosed()) {
            JsonToken token = jsonParser.nextToken();

            if (JsonToken.FIELD_NAME.equals(token)) {
                String fieldname = jsonParser.getCurrentName();
                int c = 0;
                switch (fieldname) {
                    case PASSWORD:
                        jsonParser.nextToken();
                        password = jsonParser.getValueAsString();
                        break;
                    case DICTIONARY_PROGRESS:
                        jsonParser.nextToken();
                        c = 0;
                        while (jsonParser.nextToken() != JsonToken.END_ARRAY) { if(c>3)throw new JsonParseException(jsonParser, "Unable to load JSON data");
                            gamedata.setDictionaryProgress(c++, Boolean.parseBoolean(jsonParser.getValueAsString()));
                        }
                        break;
                    case NAMES_PROGRESS:
                        jsonParser.nextToken();
                        c = 0;
                        while (jsonParser.nextToken() != JsonToken.END_ARRAY) {if(c>3)throw new JsonParseException(jsonParser, "Unable to load JSON data");
                            gamedata.setNamesProgress(c++, Boolean.parseBoolean(jsonParser.getValueAsString()));
                        }
                        break;
                    case PLACES_PROGRESS:
                        jsonParser.nextToken();
                        c = 0;
                        while (jsonParser.nextToken() != JsonToken.END_ARRAY) {if(c>3)throw new JsonParseException(jsonParser, "Unable to load JSON data");
                            gamedata.setShakespearProgress(c++, Boolean.parseBoolean(jsonParser.getValueAsString()));
                        }
                        break;
                    case DICTIONARY_BEST:
                        jsonParser.nextToken();
                        c = 0;
                        while (jsonParser.nextToken() != JsonToken.END_ARRAY) { if(c>3)throw new JsonParseException(jsonParser, "Unable to load JSON data");
                            gamedata.setDictionaryBest(c++, Integer.parseInt(jsonParser.getValueAsString()));
                        }
                        break;
                    case NAMES_BEST:
                        jsonParser.nextToken();
                        c = 0;
                        while (jsonParser.nextToken() != JsonToken.END_ARRAY) {if(c>3)throw new JsonParseException(jsonParser, "Unable to load JSON data");
                            gamedata.setNamesBest(c++, Integer.parseInt(jsonParser.getValueAsString()));
                        }
                        break;
                    case PLACES_BEST:
                        jsonParser.nextToken();
                        c = 0;
                        while (jsonParser.nextToken() != JsonToken.END_ARRAY) {if(c>3)throw new JsonParseException(jsonParser, "Unable to load JSON data");
                            gamedata.setShakespearBest(c++, Integer.parseInt(jsonParser.getValueAsString()));
                        }
                        break;

                    default:
                        throw new JsonParseException(jsonParser, "Unable to load JSON data");
                }
            }
        }

        if(password.equals(data.getPassword())){
            return true;
        }
        return false;
    }


    public void exportData(GameData data, Path filePath) throws IOException { }
}

