package data;

import apptemplate.AppTemplate;
import buzzword.Buzzword;
import com.fasterxml.jackson.core.*;
import components.AppDataComponent;
import components.AppFileComponent;


import java.io.IOException;
import java.io.OutputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import java.util.stream.Stream;

/**
 * Created by Christopher Delvalle on 11/13/16.
 */
public class GameData{// implements AppDataComponent {
    private String username;
    private String password;
    private boolean[] DictionaryProgress;
    private boolean[] NamesProgress;
    private boolean[] ShakespearProgress;

    private int[] DictionaryBest;
    private int[] NamesBest;
    private int[] ShakespearBest;

    //gameplay data
    private LetterGraph graph;
    private int scoreCap;
    private int targetScore;
    private int score;

    private HashSet<String> wordsGuessed;
    private HashSet<String> possibleWords;

    private Buzzword app;

    public GameData(Buzzword app) {
        this.app = app;
        DictionaryProgress = new boolean[4];
        NamesProgress = new boolean[4];
        ShakespearProgress = new boolean[4];

        DictionaryBest = new int[4];
        NamesBest = new int[4];
        ShakespearBest = new int[4];
    }

    public boolean[][] getProgress(){
        boolean[][] progress = {DictionaryProgress, NamesProgress, ShakespearProgress};
        return progress;
    }
    public int[][] getBest(){
        int[][] best = {DictionaryBest, NamesBest, ShakespearBest};
        return best;
    }
    public String getUsername(){
        return username;
    }
    public String getPassword(){
        return password;
    }
    public void setUsername(String username){
        this.username = username;
    }
    public void setPassword(String password){
        this.password = password;
    }
    public void setDictionaryProgress(int index, boolean status){
        DictionaryProgress[index] = status;

    }
    public void setNamesProgress(int index, boolean status){
        NamesProgress[index] = status;
    }
    public void setShakespearProgress(int index, boolean status){
        ShakespearProgress[index] = status;
    }

//generation
    public void generateGrid(Dictionary dictionary){
        graph = new LetterGraph();
        graph.generateLetters(dictionary);
        possibleWords = graph.solve(dictionary);
        scoreCap = graph.getScoreCap();
        targetScore = (int)(scoreCap * ((10.0+app.level*6.0)/100));
        wordsGuessed = new HashSet<>();
        System.out.println("Target score:" + targetScore);
//        test.findLongestChain(test.graph.get(0));

    }
    public LetterGraph getGraph(){
        return graph;
    }
    public int getTargetScore(){
        return targetScore;
    }
    public int getScoreCap(){
        return scoreCap;
    }


//game over reset data
    public void gameReset(){

    }
//logout reset data
    public void reset() {

    }

    public boolean guessWord(String word){
        if(possibleWords.contains(word)&&!wordsGuessed.contains(word)){
            wordsGuessed.add(word);
            return true;
        }
        return false;

    }

    public HashSet<String> getPossibleWords() {
        return possibleWords;
    }

    public void setPossibleWords(HashSet<String> possibleWords) {
        this.possibleWords = possibleWords;
    }

    public boolean setDictionaryBest(int index, int score){
        if(DictionaryBest[index]<=score) {
            DictionaryBest[index] = score;
            return true;
        }
        return false;
    }

    public boolean setNamesBest(int index, int score){
        if(NamesBest[index]<=score) {
            NamesBest[index] = score;
            return true;
        }
        return false;
    }

    public boolean setShakespearBest(int index, int score){
        if(ShakespearBest[index]<=score) {
            ShakespearBest[index] = score;
            return true;
        }
        return false;
    }

    public int getDictionaryBest(int index){
        return DictionaryBest[index];
    }
    public int getNamesBest(int index){
        return NamesBest[index];
    }
    public int getShakespearBest(int index){
        return ShakespearBest[index];
    }
}
