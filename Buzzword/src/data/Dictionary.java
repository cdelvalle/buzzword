package data;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

public class Dictionary
{
    private Set<String> wordsSet;
    private String dictionary;
    public Dictionary(String dictionary) //throws IOException
    {
        this.dictionary = dictionary;

        Path path = Paths.get(getClass().getClassLoader().getResource("words/"+dictionary+".txt").getPath());
        byte[] readBytes = new byte[0];
        try {
            readBytes = Files.readAllBytes(path);

            String wordListContents = null;

            wordListContents = new String(readBytes, "UTF-8");

            String[] words = wordListContents.toLowerCase().split("\n");
            wordsSet = new HashSet<>();
            Collections.addAll(wordsSet, words);

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public Set<String> getWordsSet(){
        return wordsSet;
    }

    public boolean contains(String word)
    {
        return wordsSet.contains(word);
    }

//    public String getRandom(int maxLength){
//        Random r = new Random();
//
//
//        return "";
//    }
}