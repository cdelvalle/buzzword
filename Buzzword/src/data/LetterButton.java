

package data;
import javafx.scene.control.Button;
import javafx.scene.shape.Rectangle;

import java.util.ArrayList;
import java.util.HashSet;

///**
// * Created by Christopher Delvalle on 12/7/16.
// */
public class LetterButton {
    private Button btn;
    private LetterNode node;
    private ArrayList<Rectangle> connectors;
//
    public LetterButton(Button btn, LetterNode node) {
        connectors = new ArrayList<>();
        this.btn = btn;
        this.node = node;
        node.setLetterButton(this);
    }
    public Button getButton(){
        return btn;
    }
    public void setButton(Button btn){
        this.btn = btn;
    }

    public LetterNode getNode(){
        return node;
    }

    public void setNode(LetterNode node){

    }

    public void addConnector(Rectangle connector){
        connectors.add(connector);
    }
    public ArrayList<Rectangle> getConnectors(){
        return connectors;
    }
    public Rectangle hasCommonConnector(LetterButton btn){
        for(Rectangle i: connectors){
            for(Rectangle j: btn.getConnectors()){
                if(i==j){
                    return i;
                }
            }
        }
        return null;
    }
//
//    }
//    public void setNode(LetterNode letterNode){
//        this.letterNode = letterNode;
//        letterNode.setLetterButton(this);
//    }
//
//    public LetterNode getNode(){
//        return letterNode;
//    }
//    public Rectangle getLeftConnector(){
//        return leftConnector;
//    }
//    public void setRightConnector(Rectangle rightConnector){
//        this.rightConnector = rightConnector;
//    }
//    public Rectangle getBottomConnector(){
//        return bottomConnector;
//    }
//
//    public Rectangle getTopConnector(){
//        return topConnector;
//    }
//
//    public void setBottomConnector(Rectangle bottomConnector){
//        this.bottomConnector = bottomConnector;
//    }
//
//    public void setupRightConnectors(LetterButton l1, LetterButton l2, LetterButton l3){
//        if(l1.getNode().getRight()!=null)
//            l1.setRightConnector(l1.getNode().getRight().getLetterButton().getLeftConnector());
//        if(l2.getNode().getRight()!=null)
//            l2.setRightConnector(l2.getNode().getRight().getLetterButton().getLeftConnector());
//        if(l3.getNode().getRight()!=null)
//            l3.setRightConnector(l3.getNode().getRight().getLetterButton().getLeftConnector());
//    }
//    public void setupBottomConnectors(LetterButton l1, LetterButton l2, LetterButton l3){
//        if(l1.getNode().getBottom()!=null)
//            l1.setBottomConnector(l1.getNode().getBottom().getLetterButton().getTopConnector());
//        if(l2.getNode().getBottom()!=null)
//            l2.setBottomConnector(l2.getNode().getBottom().getLetterButton().getTopConnector());
//        if(l3.getNode().getBottom()!=null)
//            l3.setBottomConnector(l3.getNode().getBottom().getLetterButton().getTopConnector());
//    }
//
//
//
}
