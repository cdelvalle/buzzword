package data;

import java.util.Comparator;

/**
 * Created by irk on 12/11/16.
 */

public class LengthSort implements Comparator<String> {
        @Override
        public int compare(String o1, String o2) {
            if (o1.length()!=o2.length()) {
                return o1.length()-o2.length(); //overflow impossible since lengths are non-negative
            }
            return o1.compareTo(o2);
        }
}

