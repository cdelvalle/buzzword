package data;

import java.io.IOException;
import java.lang.reflect.Array;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import java.util.stream.Stream;

/**
 * Created by irk on 11/28/16.
 */
public class LetterGraph {
    private ArrayList<LetterNode> graph;
    private ArrayList<LetterNode> unused;
    private HashSet<String> wordsChosen;
//    private Dictionary dictionaryWords;
//    private Dictionary names;
//    private Dictionary places;
    final String alphabet = "eeeeeeeeeeeeetttttttttaaaaaaaaooooooooiiiiiiinnnnnnnsssssshhhhhhrrrrrrddddllllcccuuummwwffggyyppbvkjxqz";
    int scoreCap = 0;
    LetterGraph(){
//        dictionaryWords = new Dictionary("DictionaryWords");
        graph = new ArrayList<>();
        unused = new ArrayList<>();
        wordsChosen = new HashSet<>();
        for(int i = 0; i<16; i++) {
            LetterNode node = new LetterNode('0', null, null, null, null);
            graph.add(node);
            unused.add(node);
        }
        for(int j = 0; j<4; j++){
            for(int i = 0; i<3;i++){
                graph.get(4*j+i).setRight(graph.get(4*j+i+1));
                graph.get(4*j+i+1).setLeft(graph.get(4*j+i));
            }
        }
        for(int j = 0;j<4;j++) {
            for (int i = 0; i < 3; i++) {
                graph.get(4 * i+j).setBottom(graph.get(4 * (i + 1)+j));
                graph.get(4 * (i + 1)+j).setTop(graph.get(4 * i+j));
            }
        }
    }
//    public void randomizeLetters(){
//        Random r = new Random();
//        for(LetterNode node: graph){
//            node.setLetter(alphabet.charAt(r.nextInt(alphabet.length())));
//        }
//    }
//    public String getWordOld(int maxLength,String dictionary){
//        URL wordsResource = getClass().getClassLoader().getResource("words/"+dictionary+".txt");
//        assert wordsResource != null;
//        String word;
//        while (true) {
//            int toSkip = new Random().nextInt(132431);
////            word = dictionaryWords.getWordsSet();
//            System.out.println(toSkip);
//            try (Stream<String> lines = Files.lines(Paths.get(wordsResource.toURI()))) {
//                word = lines.skip(toSkip).findFirst().get();
//                if(word.length()<=maxLength)return word;
//            } catch (IOException | URISyntaxException e) {
//                e.printStackTrace();
//                System.exit(1);
//            }
//        }
//    }

    public String getWord(int maxLength, Dictionary dictionary){
//        ArrayList
        ArrayList<String> words = new ArrayList<>();

        words.addAll(dictionary.getWordsSet());
        while(true){
            String word = words.get(new Random().nextInt(words.size()));
            if(word.length()<=maxLength&&!wordsChosen.contains(word)) {
                wordsChosen.add(word);
                return word;
            }
        }
    }

//    public void generateLettersOld(){
//        Random r = new Random();
//        while(!unused.isEmpty()){//&&unused.size()>2) {
//            LetterNode root = unused.get(r.nextInt(unused.size()));
//
//            int maxWordLength = findLongestChain(root);
//            if(maxWordLength<3)break;
//
//            String word = getWord(maxWordLength,dictionaryWords);
//            System.out.println("<"+word+">");
//            scoreCap += 10+5*(word.length()-3);
//            System.out.println(scoreCap);
//            addWord(root, word);
//        }
//        for(LetterNode node: unused){
//            node.setLetter(alphabet.charAt(r.nextInt(alphabet.length())));
//        }
//    }

    public void generateLetters(Dictionary dictionary){
        scoreCap = 0;
        Random r = new Random();

        Set<Character> successfulCharacters = new HashSet<Character>();
        int count = 0;
        while(!unused.isEmpty()) {
            String word = getWord(16, dictionary);

            //find word that uses an already used letter
            boolean usesOldLetter = false;
            if (!successfulCharacters.isEmpty())
                for (int i = 0; i < word.length(); i++) {
                    if (successfulCharacters.contains(word.charAt(i)))
                        usesOldLetter = true;
                    break;
                }

            //try and find place to put word
            if (usesOldLetter||successfulCharacters.isEmpty()){
                boolean success = false;
                for (LetterNode root : graph) {
                    if (root.getLetter() == word.charAt(0)||((root.getLetter() == '0')&&successfulCharacters.isEmpty())) {
                        success = addWord(root, word);
                        if (success)
                            System.out.println("\nadded word: " + word + "");
                    }
                    if (success) {
                        for (int i = 0; i < word.length(); i++) {
                            successfulCharacters.add(word.charAt(i));
                        }
                        break;
                    }
                }
//                if (!success)
//                    System.out.println("failed to add word " + word);
            }
//            else{
//                System.out.println("ignored "+word);
//            }
            if (unused.size() < 3) break;
            count++;
            System.out.print(count+"...");
            if(count>500)//dictionary.getWordsSet().size())//failafe that might work? I'm not entirely sure
            {
                generateLetters(dictionary);
                return;
            }
        }

            for (LetterNode node : unused) {
                node.setLetter(alphabet.charAt(r.nextInt(alphabet.length())));
            }
    }
    private boolean addWord(LetterNode root, String word){
        ArrayList<LetterNode> list;// = new ArrayList<>();
        list = findPath(root, word);//REMEMBER:changed this to string instead of ind
        int i = 0;
        if(list!=null) {
            for (LetterNode node : list) {
                System.out.print(node.getLetter()+"->"+word.charAt(i)+" | ");
                node.setLetter(word.charAt(i));
                node.setSelected(true);
                unused.remove(node);
                i++;
            }

            return true;
        }

        return false;
    }

    public static ArrayList findSelections(LetterNode root, String word){
        ArrayList<ArrayList<LetterNode>> paths= new ArrayList<>();
        findSelections(root, word.length(), paths, new ArrayList<LetterNode>(),word);

        if(!paths.isEmpty())
            return paths;
        else
            return null;
    }

    private static void findSelections(LetterNode root, int length, ArrayList paths, ArrayList path,String word){
        path.add(root);
        if(path.size()==length){
            paths.add(path);
            return;
        }else {
            if (root.getTop() != null && !path.contains(root.getTop())&& (root.getTop().getLetter() == word.charAt(path.size()))) {
                findSelections(root.getTop(), length, paths, (ArrayList) path.clone(), word);
            }
            if (root.getBottom() != null && !path.contains(root.getBottom())&& (root.getBottom().getLetter() == word.charAt(path.size()))) {
                findSelections(root.getBottom(), length, paths, (ArrayList) path.clone(), word);
            }
            if (root.getLeft() != null && !path.contains(root.getLeft())&& (root.getLeft().getLetter() == word.charAt(path.size()))) {
                findSelections(root.getLeft(), length, paths, (ArrayList) path.clone(), word);
            }
            if (root.getRight() != null && !path.contains(root.getRight())&& (root.getRight().getLetter() == word.charAt(path.size()))) {
                findSelections(root.getRight(), length, paths, (ArrayList) path.clone(), word);
            }

        }
        root.setSelected(false);

    }

    private ArrayList findPath(LetterNode root, String word){
        Random r = new Random();

        ArrayList<ArrayList> paths= new ArrayList<>();
//        ArrayList<LetterNode> path =
        findPath(root, word.length(), paths, new ArrayList<LetterNode>(),word);

        if(!paths.isEmpty())//I dont think this is alwasy true...
            return paths.get(r.nextInt(paths.size()));
        else
            return null;
    }
    private void findPath2(LetterNode root, int length, ArrayList paths, ArrayList path,String word){
        path.add(root);
        root.setSelected(true);
        if(path.size()==length){
            paths.add(path);
            root.setSelected(false);
            return;
        }else {
//            if (root.getTop() != null||root.getBottom() != null||root.getLeft() != null||root.getRight() != null) {!path.contains(root.getTop())&&
                if (root.getTop() != null && !root.getTop().getSelected() ) {
                    findPath(root.getTop(), length, paths, (ArrayList) path.clone(), word);
                }
                if (root.getBottom() != null && !path.contains(root.getBottom())&& (!root.getBottom().getSelected() || root.getBottom().getLetter() == word.charAt(path.size()))) {
                    findPath(root.getBottom(), length, paths, (ArrayList) path.clone(), word);
                }
                if (root.getLeft() != null && !path.contains(root.getLeft())&& (!root.getLeft().getSelected() || root.getLeft().getLetter() == word.charAt(path.size()))) {
                    findPath(root.getLeft(), length, paths, (ArrayList) path.clone(), word);
                }
                if (root.getRight() != null && !path.contains(root.getRight())&& (!root.getRight().getSelected() || root.getRight().getLetter() == word.charAt(path.size()))) {
                    findPath(root.getRight(), length, paths, (ArrayList) path.clone(), word);
                }
//            }
//            else{
////                paths.add(null);
////                paths=null;
//            }
        }
        root.setSelected(false);

    }

    private void findPath(LetterNode root, int length, ArrayList paths, ArrayList path,String word){
        path.add(root);
//        root.setSelected(true);
        if(path.size()==length){
            paths.add(path);
//            root.setSelected(false);
            return;
        }else {
//            if (root.getTop() != null||root.getBottom() != null||root.getLeft() != null||root.getRight() != null) {!path.contains(root.getTop())&&
            if (root.getTop() != null && !path.contains(root.getTop())&& (root.getTop().getLetter() == '0' || root.getTop().getLetter() == word.charAt(path.size()))) {
                findPath(root.getTop(), length, paths, (ArrayList) path.clone(), word);
            }
            if (root.getBottom() != null && !path.contains(root.getBottom())&& (root.getBottom().getLetter() == '0' || root.getBottom().getLetter() == word.charAt(path.size()))) {
                findPath(root.getBottom(), length, paths, (ArrayList) path.clone(), word);
            }
            if (root.getLeft() != null && !path.contains(root.getLeft())&& (root.getLeft().getLetter() == '0' || root.getLeft().getLetter() == word.charAt(path.size()))) {
                findPath(root.getLeft(), length, paths, (ArrayList) path.clone(), word);
            }
            if (root.getRight() != null && !path.contains(root.getRight())&& (root.getRight().getLetter() == '0' || root.getRight().getLetter() == word.charAt(path.size()))) {
                findPath(root.getRight(), length, paths, (ArrayList) path.clone(), word);
            }
//            }
//            else{
////                paths.add(null);
////                paths=null;
//            }
        }
        root.setSelected(false);

    }

    public int findLongestChain(LetterNode root){
//        int top = 1;
//        int bottom = 1;
//        int left = 1;
//        int right = 1;
//
//        root.setSelected(true);
//
//        if(root.getTop()!=null&&!root.getTop().getSelected()){
//            findLongestChain(root, 2);
//        }
//        if(root.getBottom()!=null&&!root.getBottom().getSelected()){
//            findLongestChain(root, 2);
//        }
//        if(root.getLeft()!=null&&!root.getLeft().getSelected()){
//            findLongestChain(root, 2);
//        }
//        if(root.getRight()!=null&&!root.getRight().getSelected()){
//            findLongestChain(root, 2);
//        }
//
//        root.setSelected(false);
//        return Math.max(top,Math.max(bottom,Math.max(left,right)));
        return findLongestChain(root, 1);
    }
    private int findLongestChain(LetterNode root, int count){
//        System.out.println(count);
        int top = count;
        int bottom = count;
        int left = count;
        int right = count;
        root.setSelected(true);
        if(root.getTop()!=null&&!root.getTop().getSelected()){
            top = findLongestChain(root.getTop(), count+1);
        }
        if(root.getBottom()!=null&&!root.getBottom().getSelected()){
            bottom = findLongestChain(root.getBottom(), count+1);
        }
        if(root.getLeft()!=null&&!root.getLeft().getSelected()){
            left = findLongestChain(root.getLeft(), count+1);
        }
        if(root.getRight()!=null&&!root.getRight().getSelected()){
            right = findLongestChain(root.getRight(), count+1);
        }


        root.setSelected(false);
        return Math.max(top,Math.max(bottom,Math.max(left,right)));
    }

    public LetterNode getNode(int i){
        return graph.get(i);
    }
    public int getScoreCap(){
        return scoreCap;
    }

    public HashSet<String> solve(Dictionary dictionary){
        HashSet<String> words = new HashSet<>();
        deselectAll();
        for(LetterNode node: graph){
            solveNode(node, dictionary,"",words);
        }
        for(String s: words){
            int temp = scoreCap;

            if(s.length()<=4)
                scoreCap+=1;
            else if(s.length()==5)
                scoreCap+=2;
            else if(s.length()==6)
                scoreCap+=3;
            else  if(s.length()==7)
                scoreCap+=5;
            else
                scoreCap+=11;
            System.out.println(scoreCap-temp+"<"+s+">");

        }
        System.out.println("Max score: "+scoreCap);
        return words;
    }
    private void solveNode(LetterNode node, Dictionary dictionary, String word, HashSet<String> words){
        node.setSelected(true);
        word = word + node.getLetter();

        if(word.length()>=3&&dictionary.contains(word))
                words.add(word);
        for(LetterNode neighbor: node.getNodes()){
            if(neighbor!=null&&!neighbor.getSelected()){
                solveNode(neighbor,dictionary,word,words);
            }
        }

        node.setSelected(false);
    }
    public void deselectAll(){
        for(LetterNode node: graph){
            node.setSelected(false);
        }
    }
}
