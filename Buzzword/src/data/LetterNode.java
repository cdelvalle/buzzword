package data;

import javafx.scene.shape.Rectangle;

import java.util.ArrayList;

/**
 * Created by irk on 11/28/16.
 */
public class LetterNode {
    private boolean selected;
    private char letter;
    private LetterButton letterButton;
    private LetterNode top;
    private LetterNode bottom;
    private LetterNode left;
    private LetterNode right;
//    private LetterButton letterButton;

    public LetterNode(char letter,LetterNode top,LetterNode bottom,LetterNode left,LetterNode right){
        this.top = top;
        this.bottom = bottom;
        this.left = left;
        this.right = right;
        this.letter = Character.toLowerCase(letter);
        selected = false;
    }
    public boolean toggleSelected(){
        selected = !selected;
        return selected;
    }

    public void setSelected(boolean selected){
        this.selected = selected;
    }

    public LetterNode getTop(){
        return top;
    }
    public LetterNode getBottom(){
        return bottom;
    }
    public LetterNode getLeft(){
        return left;
    }
    public LetterNode getRight(){
        return right;
    }

    public void setTop(LetterNode top){
        this.top = top;
    }
    public void setBottom(LetterNode bottom){
        this.bottom = bottom;
    }
    public void setLeft(LetterNode left){
        this.left = left;
    }
    public void setRight(LetterNode right){
        this.right = right;
    }

    public void setLetter(char letter){
        this.letter = Character.toLowerCase(letter);
    }

    public char getLetter(){
        return letter;
    }

    public boolean getSelected(){
        return selected;
    }

    public boolean contains(LetterNode node){
        if(node == left||node == right||node == top||node == bottom){
            return true;
        }
        else{
            return false;
        }
    }

    public ArrayList<LetterNode> getNodes(){
        ArrayList<LetterNode> nodes = new ArrayList<>();
        nodes.add(top);
        nodes.add(bottom);
        nodes.add(left);
        nodes.add(right);
        return nodes;
    }
    public void setLetterButton(LetterButton letterButton){
        this.letterButton = letterButton;
    }
    public LetterButton getLetterButton(){
        return letterButton;
    }

//    public LetterButton getLetterButton(){
//        return letterButton;
//    }
//
//    public void setLetterButton(LetterButton letterButton){
//        this.letterButton = letterButton;
////        letterButton.setNode(this);
//    }
}
