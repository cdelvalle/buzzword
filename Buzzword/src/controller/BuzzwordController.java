package controller;

import buzzword.Buzzword;
import data.GameData;
import javafx.application.Application;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;

import java.io.IOException;
import java.rmi.server.ExportException;
import java.util.Optional;

/**
 * Created by Christopher Delvalle on 11/13/16.
 */
public class BuzzwordController{
    public Buzzword app;
    public GameData data;
//    private GameData gameData;
    public BuzzwordController(Buzzword app) {
        this.app = app;
        data = app.getData();
    }

    public void handleCreateRequest(){
        app.createLayout();
    }

    public void handleLoginRequest(){
        app.loginLayout();
//        System.out.println("login");
    }

    public void handleLogoutRequest(){
        app.setTimerAllowed(false);
        app.resetData();
        app.defaultLayout();

    }

    public void handleHomeRequest(){
        app.setTimerAllowed(false);
        app.homeLayout();
    }

    public void handleProfileRequest(){
        app.setTimerAllowed(false);
        app.profileLayout();
    }

    public void handleQuitRequest(){
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Confirm exit");
        alert.setHeaderText("You are about to exit the application.");
        alert.setContentText("Are you sure you want to exit?");

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK){
            System.exit(0);
        } else {

        }

    }

    public void handleModeRequest(){
        app.modeLayout();
//        System.out.println("mode");
    }

    public void handlePlayRequest(){
        app.levelSelectLayout();
//        System.out.println("play");

    }

    public void handlePauseRequest(){
//        app.hideNodes();
//        System.out.println("pause");

    }

    public void handleStartRequest(){
        app.gameplayLayout();
//        System.out.println("gameplay");
    }

    public boolean handleNewUser(){
        data = app.getData();
        app.getFile().saveData(data);
        return true;
    }

    public boolean handleReturningUser(){
        data = app.getData();
        try{
            return app.getFile().loadData(data);
        }
        catch(Exception e){
            return false;
        }
    }

}
