package gui;

import apptemplate.AppTemplate;
import components.AppWorkspaceComponent;
import controller.BuzzwordController;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ToolBar;
import javafx.scene.layout.*;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;
import propertymanager.PropertyManager;
import ui.AppGUI;

import java.io.IOException;

import static buzzword.BuzzwordProperties.*;

/**
 * This class serves as the GUI component for the Hangman game.
 *
 * @author Christopher Delvalle
 */
public class Workspace extends AppWorkspaceComponent {

    AppTemplate app; // the actual application
    AppGUI      gui; // the GUI inside which the application sits

    BuzzwordController controller;



    /**
     * Constructor for initializing the workspace, note that this constructor
     * will fully setup the workspace user interface for use.
     *
     * @param initApp The application this workspace is part of.
     * @throws IOException Thrown should there be an error loading application
     *                     data for setting up the user interface.
     */
    public Workspace(AppTemplate initApp) throws IOException {
//        app = initApp;
//        gui = app.getGUI();
//        controller = (BuzzwordController) gui.getFileController();    //new HangmanController(app, startGame); <-- THIS WAS A MAJOR BUG!??
//        layoutGUI();     // initialize all the workspace (GUI) components including the containers and their layout
//        setupHandlers(); // ... and set up event handling

    }

    private void layoutGUI() {

    }

    private void setupHandlers() {



    }

    /**
     * This function specifies the CSS for all the UI components known at the time the workspace is initially
     * constructed. Components added and/or removed dynamically as the application runs need to be set up separately.
     */
    @Override
    public void initStyle() {

    }

    @Override
    public void reloadWorkspace() {

    }

}